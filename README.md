# java-idea-bookmanager

#### 介绍
基于Swing的java 图书管理系统（简版）框架程序。

#### 软件架构
软件架构说明

基于C/S架构 + Java Swing

#### 安装教程

1.  配置MySQL 5.5以上数据库系统    
2.  连接数据库,创建 bookmanager 数据库,导入doc/bookmanager.sql    
3.  执行查询，创建相关表并插入数据  

#### 使用说明

1. 运行 JFrame/LoginFrame.java

![](./doc/img/login1.png)

- 用户名  admin 密码 111111

![](./doc/img/userframe.png)

#### 实训任务

1. 分析理解现有项目结构，优化页面，并完成多语言图形界面设计；    
2. 增加提供选择语言选择主界面功能；    
3. 理解数据库访问的单元测试方法，并编写一张表的测试方法； 
4. 掌握代码管理的方法并进行分享；
5. 编写实训报告，说明封装、继承、多态、异常、集合技术的具体应用。